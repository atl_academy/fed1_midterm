To work on the following project, clone this repository, then create your own branch and give your name as a branch name. Write all your code in that branch. After you complete the task commit your changes and push the branch to remote server.

Building Tic Tac Toe game.

Take 5 mins to read the following article and understand how this game works: https://en.wikipedia.org/wiki/Tic-tac-toe

I strongly suggest using React with either Hooks or Redux for state management (to simplify your work) but you can also implement this task with only HTML/CSS/JS.

Tasks:
1. Create a board with 9 cells. (1 point)

2. Add the JS functionality which lets cells to be clicked on and writes either "X" or "O" into the cell. (1 point)

3. Add the JS functionality which lets 2 players play the game. "X" and "O" should be written into cells one after another. First begin with "X". (1 point)

4. Write the JS function which at each step of the game checks whether either of players won so far. If any of the players is won the game cannot be continued and has to be restarted. (1 point)

5. Add restart button, which when clicked restarts the game. (1 point)

Bonus: the most beautiful design will receive extra 2 points (colorful, with shadows, animations etc.)

* For design inspiration look for: https://dribbble.com/search/tic%20tac%20toe
* For "X" icons: https://www.flaticon.com/free-icons/x
* For "O" icons: https://www.flaticon.com/free-icons/o
